//
//  AddCollectionViewCell.swift
//  DragCollection
//
//  Created by 江俊瑩 on 2021/3/17.
//

import UIKit

class AddCollectionViewCell: UICollectionViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.cornerRadius = 35
    }

}
