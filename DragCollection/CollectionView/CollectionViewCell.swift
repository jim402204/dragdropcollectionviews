//
//  CollectionViewCell.swift
//  DragCollection
//
//  Created by 江俊瑩 on 2021/3/17.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var numLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        self.layer.cornerRadius = 35
    }

}
