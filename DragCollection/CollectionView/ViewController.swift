//
//  ViewController.swift
//  DragCollection
//
//  Created by 江俊瑩 on 2021/3/17.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    let collectionViewReuseID = String(describing: CollectionViewCell.self)
    let addCollectionViewReuseID = String(describing: AddCollectionViewCell.self)
    
    var models = Array(1...8) + [999]
    
    
    var selectedIndexRow = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configtrueCollectionView()
        
        configTitleView()
    }

}

extension ViewController {
    
    func configTitleView() {
        
        let view = UIView(frame: .init(origin: .zero, size: CGSize(width: 140, height: 40)))
        
        let titleView = UIImageView(image: #imageLiteral(resourceName: "logo"))
        titleView.frame.size = CGSize(width: 120, height: 40)
        
        view.addSubview(titleView)
        titleView.translatesAutoresizingMaskIntoConstraints = false
        
        let x = titleView.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        let y = titleView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        let h = titleView.heightAnchor.constraint(equalToConstant: 40)
        let w = titleView.widthAnchor.constraint(equalToConstant: 120)
        NSLayoutConstraint.activate([x,y,w,h])
        
        self.navigationItem.titleView = view
    }
    
    func configtrueCollectionView() {
        
        collectionView.delegate = self
        collectionView.dataSource = self
        let nib = UINib(nibName: collectionViewReuseID, bundle: .main)
        collectionView.register(nib, forCellWithReuseIdentifier: collectionViewReuseID)
        
        let nib2 = UINib(nibName: addCollectionViewReuseID, bundle: .main)
        collectionView.register(nib2, forCellWithReuseIdentifier: addCollectionViewReuseID)
        
//        storyboard 估計值要打開 寬度約束優先權設定變低
        codeLayout()
        
        collectionDragConfig()
    }
    
    func codeLayout() {
        
        
        let screenWidth = UIScreen.main.bounds.width
        
        var itemWidth = (screenWidth - (12 * 2) - (2 * 9))/3
        // 去掉小數位
        itemWidth -= 1
        
        print("itemWidth: \(itemWidth)")
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 12, bottom: 12, right: 12)
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 10
        layout.estimatedItemSize = .zero

//        let length = floor((UIScreen.main.bounds.width - (2 * (20 + 10)) - (10 * 2)) / 3)
        
        let length = itemWidth
        layout.itemSize = CGSize(width: length, height: length)
        layout.scrollDirection = .vertical
        collectionView.collectionViewLayout = layout
        
        
    }
    
    func collectionDragConfig() {
        
        collectionView.dragDelegate = self
        
        collectionView.dropDelegate = self
        
        collectionView.dragInteractionEnabled = true
    }
    
}


extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return models.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        
        if self.models[indexPath.row] == 999 {
            
            if let addItem = collectionView.dequeueReusableCell(withReuseIdentifier: addCollectionViewReuseID, for: indexPath) as? AddCollectionViewCell {
                
                return addItem
            }
        }
        
        
        if let item = collectionView.dequeueReusableCell(withReuseIdentifier: collectionViewReuseID, for: indexPath) as? CollectionViewCell {
            
            let displayNum = self.models[(indexPath.row)]
            item.numLabel.text = "\(displayNum)"
            
            return item
        }
        
        
        
        return UICollectionViewCell()
    }

    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if self.models[indexPath.row] == 999 {
            
            if self.models[indexPath.row - 1] == 0 {
                
                let findeIndex = self.models.firstIndex(of: 0) ?? 0
                
                print("findeIndex: \(findeIndex)")
                
                let cell = collectionView.cellForItem(at: IndexPath(row: findeIndex, section: 0)) as? CollectionViewCell
                
                print("cell: \(cell) \(indexPath.row)")
                
                // -1 // 0
                var newNum = indexPath.row
                
                if self.models[indexPath.row - 2] == 0 {
                    newNum = indexPath.row - 1
                }
                
                self.models[findeIndex] = newNum
                cell?.numLabel.text = "\(newNum)"
                
            } else {
                
                // -1 +3
                let last = self.models.count - 1
                self.models.remove(at: last)
                
                self.models = self.models + [(indexPath.row + 1),0,0] + [999]
                let finalLast = self.models.count - 1
                
                collectionView.reloadData()
                collectionView.scrollToItem(at: IndexPath(row: finalLast, section: 0), at: .bottom, animated: true)
            }
            
            return
        } else {
            
            let vc = UIViewController()
            vc.title = "選單"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
//        let vc = UIViewController()
//        vc.title = "選單"
//        vc.view.backgroundColor = .systemRed
//        self.present(vc, animated: true, completion: nil)
    }
    
    func addOneItem(indexPath: IndexPath) {
        
        // -1 +2
        let last = self.models.count - 1
        self.models.remove(at: last)
        
        self.models = self.models + [indexPath.row + 1] + [999]
        let finalLast = self.models.count - 1
        
        collectionView.reloadData()
        collectionView.scrollToItem(at: IndexPath(row: finalLast, section: 0), at: .bottom, animated: true)
    }
    
}

extension ViewController: UICollectionViewDragDelegate {
    
    //拖曳選中樣式
    func collectionView(_ collectionView: UICollectionView, dragPreviewParametersForItemAt indexPath: IndexPath) -> UIDragPreviewParameters? {
        
        let preview = UIDragPreviewParameters() //可設定外觀
        if let item = collectionView.cellForItem(at: indexPath) as? CollectionViewCell {
            
            preview.visiblePath = UIBezierPath(roundedRect: item.bounds, cornerRadius: 30)
            preview.backgroundColor = .blue
            
            return preview
        } else {
            return  nil
        }
    }
    
    //選中開始拖移
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        
        let selectedIndexRow = "\(indexPath.row)" as NSString //(NSObject & protocol)
        
        let itemProvider = NSItemProvider(object: selectedIndexRow)
        let dragItem = UIDragItem(itemProvider: itemProvider)
        dragItem.localObject = selectedIndexRow
        
        
        self.selectedIndexRow = indexPath.row
        
        return [dragItem] //[]  該index 回[] 表示index不可拖動
    }
    
}

extension ViewController: UICollectionViewDropDelegate {
    
    //拖曳過程
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        
        //固定的
        if let destRow = destinationIndexPath?.row {
            if self.models[destRow] == 0 || self.models[destRow] == 999 {
                return UICollectionViewDropProposal(operation: .forbidden)
            }
        }
        
        if session.localDragSession != nil, collectionView.hasActiveDrag {
            
            return UICollectionViewDropProposal(operation: .move, intent: .insertAtDestinationIndexPath) //插入到
            
        } else {  // 固定不動
            return UICollectionViewDropProposal(operation: .forbidden)
        }
        
        //copy  跨視圖
    }
    
    //拖曳結束 放下
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        
            guard
                let item = coordinator.items.first?.dragItem,
                let sourceIndexPath = coordinator.items.first?.sourceIndexPath,
                let destinationIndexPath = coordinator.destinationIndexPath,
                let entity = item.localObject
                
            else {
                return
            }
            
            let localString = entity as? String ?? "case fail"
            let rowDataIndex = Int(localString) ?? -1
            let oldmodel = self.models
            
            print("rowDataIndex: \(rowDataIndex)")
        
            //動畫
            collectionView.performBatchUpdates({
                
                self.models.remove(at: sourceIndexPath.row)
                print("self.models: \(self.models)")
                self.models.insert(oldmodel[rowDataIndex], at: destinationIndexPath.row)
                print("self.models: \(self.models)")
                collectionView.deleteItems(at: [sourceIndexPath])
                collectionView.insertItems(at: [destinationIndexPath])
            })
        
            //以動畫形式移動item (可以跨試圖 執行動畫)
            coordinator.drop(item, toItemAt: destinationIndexPath)
            item.localObject = nil
  
    }
    
    
    
    
}


extension UIView {
    /// SwifterSwift: Anchor all sides of the view into it's superview.
    @available(iOS 9, *)
    func fillToSuperview() {
        // https://videos.letsbuildthatapp.com/
        translatesAutoresizingMaskIntoConstraints = false
        if let superview = superview {
            let left = leftAnchor.constraint(equalTo: superview.leftAnchor)
            let right = rightAnchor.constraint(equalTo: superview.rightAnchor)
            let top = topAnchor.constraint(equalTo: superview.topAnchor)
            let bottom = bottomAnchor.constraint(equalTo: superview.bottomAnchor)
            NSLayoutConstraint.activate([left, right, top, bottom])
        }
    }
}
